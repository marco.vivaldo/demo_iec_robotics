# POC Cavanna AUTONOX RLT

## Step 1: 9-13 Ottobre

- [x] Adattamento Progetto Demo con Libreria ATC;
- [x] Test Quadro Elettrico e Safety; 
- [x] Calibrazione Robot(Verificare possesso tool di calibrazione);
- [x] Test Cinematica  Jog con e senza tool; 
- [x] Test Pick&Place Statico;

## Step 2: 24-26 Ottobre

- [x] Test Pick&Place con Tracking su Belt Simulato;
- [x] Test Pick&Place su Belt Reale;
- [x] Intervento Colleghi Spagnoli;
- [x] Valutazione Prestazioni con Misure di Velocità e Coppie;
- [x] Relazione finale prestazioni robot. 

## Step 3:

- [ ] Inserire gestione coda e simulazione inserimento prodotti(Da valutare quale codice);
- [ ] Recuperare specifiche di comunicazione sistema di visione Cavanna;
- [ ] Sviluppare interfaccia di comunicazione tra sistema di visione e Camera;
- [ ] Test con prodotti veri;
- [ ] Valutazione finale e relazione tecnica.

## Step 4:

- [ ] Formazione Ufficio Tecnico Cavanna;
- [ ] Sviluppo Applicativo con approccio Modulare(Multi CPU);
- [ ] Supporto prima applicazione;
- [ ] Mantenimento e implementazione nuove feature nella Libreria;

