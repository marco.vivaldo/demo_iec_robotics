﻿using Ace.Server.Core.Sim3d.CustomMechanics;
using Ace;
using Ace.Services.Cad.CadDataRender;
using Ace.Services.NameLookup;
using Ace.Server;
using Ace.Server.Adept.Robots;
using Ace.Server.Core;
using Ace.Server.Core.Scripting.Script;
using Ace.Server.Core.Sim3d;
using Ace.Server.Core.Sim3d.Behaviors;
using Ace.Server.Core.Sim3d.Behaviors.Runtime;
using Ace.Server.Core.Sim3d.Behaviors.Models;
using Ace.Server.Core.Sim3d.PartDetection;
using Ace.Server.Core.Sim3d.MechanicalComponent;
using Ace.Server.Xpert.PackXpert;
using Ace.Server.Xpert.PackXpert.Belts;
using Ace.Server.Xpert.PackXpert.Calibrations;
using Ace.Visualization;
using Ace.Visualization.Behaviors;
using Ace.Visualization.Hierarchies;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Ace.ShapeScript {

	/// <summary>
	/// Object defining a Shape Script
	/// </summary>
	public class ShapeScript : ExtendedShapeScriptBase {
		/// <summary>
		/// Initializes a new instance of the <see cref="ShapeScript"/> class.
		/// </summary>
		public ShapeScript() {
		}

		/// <summary>
		/// Create all Rendering Info data 
		/// </summary>
		/// <remarks>
		/// The rendering data should be created once. Drawing resources 
		/// are generated for each new instance of render information that
		/// is generated.
		/// </remarks>
		public override IEnumerable<ShapeRenderInfo> CreateRenderInfo() {
			this.IsInitialized = false;

			// Create the Shapes for rendering once in the constructor
			List<ShapeRenderInfo> renderInfoList = new List<ShapeRenderInfo>();

			// Please assign your shape.
			var shape = ace["/AP1/Shape"] as IShapeBase;
			if (shape != null) {
				DefaultFunctions.CreateObject(shape, "CollisionSource", "Group1", renderInfoList);
			}

			return renderInfoList;
		}

		/// <summary>
		/// Called to render the Shape Script object 
		/// </summary>
		/// <returns>The list of shapes to render</returns>
		public override void Render(IEnumerable<ShapeRenderInfo> renderInfoList) {
			if (this.IsInitialized == false) {
				DefaultFunctions.InitializeObject(this, renderInfoList);
				return ;
			}

			CustomMechanics mec = (CustomMechanics) ace["/AP1/RTL4-1100-3kg"];

			var variableList = DefaultFunctions.CreateGetVariableList();
			
			DefaultFunctions.AddToGetVariableList("MC_Rbt2_J1.Act.Pos", "LREAL", variableList);
			DefaultFunctions.AddToGetVariableList("MC_Rbt2_J2.Act.Pos", "LREAL", variableList);
			DefaultFunctions.AddToGetVariableList("MC_Rbt2_J3.Act.Pos", "LREAL", variableList);
			DefaultFunctions.AddToGetVariableList("MC_Rbt2_Rz.Act.Pos", "LREAL", variableList);
			
			var valueList = DefaultFunctions.GetVariableValues(this, "PLC", variableList);

			var joint1 = DefaultFunctions.GetValueFromVariableValueList("MC_Rbt2_J1.Act.Pos", valueList);
			var joint2 = DefaultFunctions.GetValueFromVariableValueList("MC_Rbt2_J2.Act.Pos", valueList);
			var joint3 = DefaultFunctions.GetValueFromVariableValueList("MC_Rbt2_J3.Act.Pos", valueList);
			var joint4 = DefaultFunctions.GetValueFromVariableValueList("MC_Rbt2_Rz.Act.Pos", valueList);
			
			mec.MovePart("UpperArm1", 0, (float)Convert.ToDouble(joint1));
			mec.MovePart("UpperArm2", 0, (float)Convert.ToDouble(joint2));
			mec.MovePart("UpperArm3", 0, (float)Convert.ToDouble(joint3));
			mec.MovePart("PlatformJoint", 0, (float)Convert.ToDouble(joint4));			

		}

	}
}
