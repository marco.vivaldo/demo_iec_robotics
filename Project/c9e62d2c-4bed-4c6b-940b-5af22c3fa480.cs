﻿using Ace;
using Ace.Services.Cad.CadDataRender;
using Ace.Services.NameLookup;
using Ace.Services.Scripting;
using Ace.Server;
using Ace.Server.Adept.Robots;
using Ace.Server.Core;
using Ace.Server.Core.Scripting.Script;
using Ace.Server.Core.Sim3d;
using Ace.Server.Core.Sim3d.PartDetection;
using Ace.Server.Core.Sim3d.MechanicalComponent;
using Ace.Server.Xpert.PackXpert;
using Ace.Server.Xpert.PackXpert.Belts;
using Ace.Server.Xpert.PackXpert.Calibrations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Ace.ShapeScript {

	/// <summary>
	/// Default Functions for Shape Scripts
	/// </summary>
	public class DefaultFunctions : ShapeScriptingBase {
	
		#region IecDataTypeList

		public static readonly Dictionary<string, Type> IecDataTypeList = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase) {
			{
				"BOOL", typeof (bool)
			}, {
				"SINT", typeof (sbyte)
			}, {
				"USINT", typeof (byte)
			}, {
				"INT", typeof (short)
			}, {
				"UINT", typeof (ushort)
			}, {
				"DINT", typeof (int)
			}, {
				"UDINT", typeof (uint)
			}, {
				"LINT", typeof (long)
			}, {
				"ULINT", typeof (ulong)
			}, {
				"BYTE", typeof (byte)
			}, {
				"WORD", typeof (ushort)
			}, {
				"DWORD", typeof (uint)
			}, {
				"LWORD", typeof (ulong)
			}, {
				"REAL", typeof (float)
			}, {
				"LREAL", typeof (double)
			}, {
				"TIME", typeof (TimeSpan)
			}, {
				"DATE", typeof (DateTime)
			}, {
				"TIME_OF_DAY", typeof (DateTime)
			}, {
				"DATE_AND_TIME", typeof (DateTime)
			}, {
				"STRING", typeof (string)
			}
		};

		#endregion

		#region VplusDataTypeList

		public static readonly Dictionary<string, Type> VplusDataTypeList = new Dictionary<string, Type>(StringComparer.Ordinal) {
			{
				"Real", typeof (double)
			}, {
				"String", typeof (string)
			}, {
				"Location", typeof (Transform3D)
			}, {
				"Precision Point", typeof (PrecisionPoint)
			}
		};

		#endregion

		public static void CreateObject(IShapeBase objectModel, string name, string collisionGroup, List<ShapeRenderInfo> renderInfoList, int PMindex = -1, bool isVision = false, string objectName = null, bool isDisplayNumber = false) {
			CheckNullArgument(objectModel, "objectModel");
			CheckNullOrEmptyArgument(name, "name");
			CheckNullOrEmptyArgument(collisionGroup, "collisionGroup");
			CheckNullArgument(renderInfoList, "renderInfoList");

			var partRenderInfo = new ShapeRenderInfo(name, objectModel, true, collisionGroup);
			partRenderInfo.HoverText = name;
			partRenderInfo.Parent = null;
			partRenderInfo.Position = new Transform3D(0, 0, 0);
			partRenderInfo.CanCollide = true;
			partRenderInfo.EnablePhysics = false;
			partRenderInfo.Visible = true;

			if (PMindex != -1) {
				partRenderInfo.PackManagerSetting.InstanceNumber = PMindex;
				partRenderInfo.DisplayNumber = PMindex;
				partRenderInfo.IsDisplayNumber = isDisplayNumber;
			}

			if (isVision) {
				if (string.IsNullOrEmpty(objectName)) {
					partRenderInfo.PackManagerSetting.PartName = objectModel.Name;
				}
				else {
					partRenderInfo.PackManagerSetting.PartName = objectName;
				}
			}

			renderInfoList.Add(partRenderInfo);

			Trace.WriteLine("Object(" + partRenderInfo.CollisionSourceName + ") initialized.");
		}

		public static void CreateMultipleObjects(IShapeBase objectModel, string name, string collisionGroup, int count, List<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(objectModel, "objectModel");
			CheckNullArgument(name, "name");
			CheckNullOrEmptyArgument(collisionGroup, "collisionGroup");
			CheckNullArgument(renderInfoList, "renderInfoList");

			for (int i = 0; i < count; i++) {
				var eachName = name + "_" + i.ToString();
				CreateObject(objectModel, eachName, collisionGroup, renderInfoList);
			}
		}

		public static void InitializeObject(IExtendedShapeScript shapeScript, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (shapeScript.IsInitialized == false) {
				foreach (var renderInfo in renderInfoList.Where(r => r.PackManagerSetting.InstanceNumber < 0)) {
					renderInfo.Visible = false;
					renderInfo.CanCollide = false;
				}
				shapeScript.IsInitialized = true;
			}
		}

		public static void LoadPart(IExtendedShapeScript shapeScript, string controllerName, string variableName, IShapeBase partModel, string name, Transform3D worldCoordinate, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(worldCoordinate, "worldCoordinate");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				foreach (var renderInfo in renderInfoList) {
					if ((renderInfo.Visible == false) && (renderInfo.Shape == partModel)) {
						if (string.IsNullOrEmpty(name)) {
							if (GetControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate) == false) {
								DisplayShapeRenderInfo(renderInfo, worldCoordinate, locationModel);
								Trace.WriteLine("Load " + renderInfo.CollisionSourceName + ".");
								SetControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate);
								break;
							}
						}
						else if (renderInfo.CollisionSourceName.Equals(name)) {
							if (GetControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate, name) == false) {
								DisplayShapeRenderInfo(renderInfo, worldCoordinate, locationModel);
								Trace.WriteLine("Load " + renderInfo.CollisionSourceName + ".");
								SetControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate, name);
								break;
							}
						}
					}
				}
			}
			else {
				RemoveControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate, name);
			}
		}

		public static void LoadPallet(IExtendedShapeScript shapeScript, string controllerName, string variableName, IShapeBase palletModel, string name, Transform3D worldCoordinate, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			LoadPart(shapeScript, controllerName, variableName, palletModel, name, worldCoordinate, locationModel, renderInfoList, variableValues);
		}

		public static void LoadPartOnPallet(IExtendedShapeScript shapeScript, string controllerName, string variableName, IShapeBase partModel, string partName, Transform3D worldCoordinate, IShapeBase palletModel, string palletName, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(worldCoordinate, "worldCoordinate");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(renderInfoList, "renderInfoList");
			
			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				ShapeRenderInfo pallet = null;
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.Shape == palletModel)) {
						if (String.IsNullOrEmpty(palletName) || renderInfo.CollisionSourceName.Equals(palletName)) {
							pallet = renderInfo;
							break;
						}
					}
				}

				if (pallet == null) {
					throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "palletName");
				}

				foreach (var renderInfo in renderInfoList) {
					if ((renderInfo.Visible == false) && (renderInfo.Shape == partModel)) {
						if (String.IsNullOrEmpty(partName)) {
							if (GetControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate) == false) {
								DisplayShapeRenderInfo(renderInfo, worldCoordinate, pallet.Parent);
								SetParentPalletCSName(renderInfo, pallet);
								Trace.WriteLine("Load " + renderInfo.CollisionSourceName + " on " + pallet.CollisionSourceName + ".");
										SetControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate);
								break;
							}
						}
						else if (renderInfo.CollisionSourceName.Equals(partName)) {
							if (GetControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate, partName) == false) {
								DisplayShapeRenderInfo(renderInfo, worldCoordinate, pallet.Parent);
								SetParentPalletCSName(renderInfo, pallet);
								Trace.WriteLine("Load " + renderInfo.CollisionSourceName + " on " + pallet.CollisionSourceName + ".");
								SetControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate, partName);
								break;
							}
						}
					}
				}
			}
			else {
				RemoveControllerBoolInfo(shapeScript, controllerName, variableName, worldCoordinate, partName);
			}
		}

		public static void UnloadPart(IExtendedShapeScript shapeScript, string controllerName, string variableName, IShapeBase partModel, string name, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(renderInfoList, "renderInfoList");
		
			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.Shape == partModel)) {
						if (String.IsNullOrEmpty(name) || renderInfo.CollisionSourceName.Equals(name)) {
							InitializeShapeRenderInfo(renderInfo);
							Trace.WriteLine("Unload " + renderInfo.CollisionSourceName + ".");
							break;
						}
					}
				}
			}
		}

		public static void UnloadPallet(IExtendedShapeScript shapeScript, string controllerName, string variableName, IShapeBase palletModel, string name, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				ShapeRenderInfo pallet = null;
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.Shape == palletModel)) {
						if (String.IsNullOrEmpty(name) || renderInfo.CollisionSourceName.Equals(name)) {
							pallet = renderInfo;
							break;
						}
					}
				}

				if (pallet != null) {
					InitializePalletShapeRenderInfo(pallet, renderInfoList);
				}
			}
		}

		public static void UnloadCollidingPart(IExtendedShapeScript shapeScript, IShapeBase partModel, IVisualizable targetModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(targetModel, "targetModel");
			CheckNullArgument(renderInfoList, "renderInfoList");
			
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Shape == partModel)) {
					if (IsCollidedToMechanicalModel(shapeScript, renderInfo, targetModel)) {
						InitializeShapeRenderInfo(renderInfo);
						Trace.WriteLine("Unload " + renderInfo.CollisionSourceName + ".");
					}
				}
			}
		}

		public static void UnloadCollidingPallet(IExtendedShapeScript shapeScript, IShapeBase palletModel, IVisualizable targetModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(targetModel, "targetModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Shape == palletModel)) {
					if (IsCollidedToMechanicalModel(shapeScript, renderInfo, targetModel)) {
						InitializePalletShapeRenderInfo(renderInfo, renderInfoList);
					}
				}
			}
		}

		public static void SetNextStep(IExtendedShapeScript shapeScript, int stepId, string controllerName, string variableName, int value, IDictionary<string, object> variableValues = null) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues) && (shapeScript.NextStep != value)) {
				Trace.WriteLine(stepId.ToString() + ": Next Step to " + value.ToString() + ".");
				shapeScript.NextStep = value;
			}
		}

		public static void PushPart(IExtendedShapeScript shapeScript, int stepId, string controllerName, string variableName, IVisualizable pushModel, IShapeBase partModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			if ((stepId != 0) && (stepId != shapeScript.NextStep)) return;
			
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(pushModel, "pushModel");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				//Push a part.
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.Shape == partModel) && (renderInfo.Parent != pushModel)) {
						if (IsCollidedToMechanicalModel(shapeScript, renderInfo, pushModel)) {
							ChangeParent(renderInfo, pushModel);
							Trace.WriteLine(stepId.ToString() + ": Push " + renderInfo.CollisionSourceName + ".");
							break;
						}
					}
				}
			}
			else {
				//Release a part.
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.Shape == partModel) && (renderInfo.Parent == pushModel)) {
						if (IsCollidedToMechanicalModel(shapeScript, renderInfo, locationModel)) {
							ChangeParent(renderInfo, locationModel);
							Trace.WriteLine(stepId.ToString() + ": Place " + renderInfo.CollisionSourceName + ".");
						}
						else {
							ChangeParent(renderInfo, null);
							Trace.WriteLine(stepId.ToString() + ": Place " + renderInfo.CollisionSourceName + " to nowhere!");
						}
							
						if (stepId != 0){
							shapeScript.NextStep++;
						}
						break;
					}
				}
			}
		}

		public static void PushPallet(IExtendedShapeScript shapeScript, int stepId, string controllerName, string variableName, IVisualizable pushModel, IShapeBase palletModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			if ((stepId != 0) && (stepId != shapeScript.NextStep)) return;
			
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(pushModel, "pushModel");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				//Push a pallet.
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.Shape == palletModel) && (renderInfo.Parent != pushModel)) {
						if (IsCollidedToMechanicalModel(shapeScript, renderInfo, pushModel)) {
							ChangePalletParent(renderInfo, pushModel, renderInfoList);
							Trace.WriteLine(stepId.ToString() + ": Push " + renderInfo.CollisionSourceName + ".");
							break;
						}
					}
				}
			}
			else {
				//Release a pallet.
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.Shape == palletModel) && (renderInfo.Parent == pushModel)) {
						if (IsCollidedToMechanicalModel(shapeScript, renderInfo, locationModel)) {
							ChangePalletParent(renderInfo, locationModel, renderInfoList);
							Trace.WriteLine(stepId.ToString() + ": Place " + renderInfo.CollisionSourceName + ".");
						}
						else {
							ChangeParent(renderInfo, null);
							Trace.WriteLine(stepId.ToString() + ": Place " + renderInfo.CollisionSourceName + " to nowhere!");
						}
							
						if (stepId != 0){
							shapeScript.NextStep++;
						}
						break;
					}
				}
			}
		}

		public static void MoveObjectOnBelt(IExtendedShapeScript shapeScript, string controllerName, string variableName, IVisualizable beltModel, Transform3D distancePerSecond, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(beltModel, "beltModel");
			CheckNullArgument(distancePerSecond, "distancePerSecond");
			CheckNullArgument(renderInfoList, "renderInfoList");
		
			var currentTime = GetCurrentControllerTime(shapeScript, controllerName, variableValues);				
			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				if (beltModel.Tag == null) {
					beltModel.Tag = currentTime;
					return;
				}
			}
			else {
				beltModel.Tag = null;
				return;
			}
				
			if (beltModel.Tag is DateTime) {
				var previousTime = (DateTime) beltModel.Tag;
				var pastSeconds = (currentTime - previousTime).TotalSeconds;
					
				foreach (var pallet in renderInfoList) {
					if (pallet.Visible && (pallet.Parent == beltModel) && string.IsNullOrEmpty(GetParentPalletCSName(pallet))) {
						if (IsCollidedToMechanicalModel(shapeScript, pallet, null) == false) {
							//Move a pallet or a part.
							var palletPos = pallet.Position;
							pallet.Position = new Transform3D(palletPos.DX + distancePerSecond.DX * pastSeconds, palletPos.DY + distancePerSecond.DY * pastSeconds, palletPos.DZ + distancePerSecond.DZ * pastSeconds, 
															palletPos.Yaw, palletPos.Pitch, palletPos.Roll);

							//Move parts on a pallet.
							foreach (var part in renderInfoList) {
								if (part.Visible && (part.Parent == beltModel) && GetParentPalletCSName(part).Equals(pallet.CollisionSourceName)) {
									var partPos = part.Position;
									part.Position = new Transform3D(partPos.DX + distancePerSecond.DX * pastSeconds, partPos.DY + distancePerSecond.DY * pastSeconds, partPos.DZ + distancePerSecond.DZ * pastSeconds,
															partPos.Yaw, partPos.Pitch, partPos.Roll);
								}
							}
						}
					}
				}

				beltModel.Tag = currentTime;
			}
		}

		public static void ClampPart(IExtendedShapeScript shapeScript, int stepId, IVisualizable robotTool, IShapeBase partModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Shape == partModel) && (renderInfo.Parent == locationModel)) {
					if (IsCollidedToMechanicalModel(shapeScript, renderInfo, robotTool)) {
						ChangeParent(renderInfo, robotTool);
						SetParentPalletCSName(renderInfo, null);
						Trace.WriteLine(stepId.ToString() + ": Clamp " + renderInfo.CollisionSourceName + ".");
						shapeScript.NextStep++;
						break;
					}
				}
			}
		}

		public static void ClampPallet(IExtendedShapeScript shapeScript, int stepId, IVisualizable robotTool, IShapeBase palletModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Shape == palletModel) && (renderInfo.Parent == locationModel)) {
					if (IsCollidedToMechanicalModel(shapeScript, renderInfo, robotTool)) {
						ChangePalletParent(renderInfo, robotTool, renderInfoList);
						Trace.WriteLine(stepId.ToString() + ": Clamp " + renderInfo.CollisionSourceName + ".");
						shapeScript.NextStep++;
						break;
					}
				}
			}
		}

		public static void ClampPartOnPallet(IExtendedShapeScript shapeScript, int stepId, IVisualizable robotTool, IShapeBase partModel, IShapeBase palletModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			ShapeRenderInfo part = null;
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Shape == partModel)) {
					if (IsCollidedToMechanicalModel(shapeScript, renderInfo, robotTool)) {
						part = renderInfo;
						break;
					}
				}
			}

			if (part != null) {
				string palletCSName = GetParentPalletCSName(part);
				foreach (var renderInfo in renderInfoList) {
					if ((renderInfo.Shape == palletModel) && renderInfo.CollisionSourceName.Equals(palletCSName)) {
						ChangeParent(part, robotTool);
						SetParentPalletCSName(part, null);
						Trace.WriteLine(stepId.ToString() + ": Clamp " + part.CollisionSourceName + " on " + palletCSName + ".");
						shapeScript.NextStep++;
						break;
					}
				}
			}
		}

		public static void ClampPartBySignal(IExtendedShapeScript shapeScript, int stepId, string controllerName, string variableName, IVisualizable robotTool, IShapeBase partModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				ClampPart(shapeScript, stepId, robotTool, partModel, locationModel, renderInfoList);
			}
		}

		public static void ClampPalletBySignal(IExtendedShapeScript shapeScript, int stepId, string controllerName, string variableName, IVisualizable robotTool, IShapeBase palletModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				ClampPallet(shapeScript, stepId, robotTool, palletModel, locationModel, renderInfoList);
			}
		}

		public static void ClampPartOnPalletBySignal(IExtendedShapeScript shapeScript, int stepId, string controllerName, string variableName, IVisualizable robotTool, IShapeBase partModel, IShapeBase palletModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues)) {
				ClampPartOnPallet(shapeScript, stepId, robotTool, partModel, palletModel, renderInfoList);
			}
		}

		public static void ReleasePart(IExtendedShapeScript shapeScript, int stepId, IVisualizable robotTool, IShapeBase partModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			ShapeRenderInfo part = null;
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Shape == partModel) && (renderInfo.Parent == robotTool)) {
					part = renderInfo;
					break;
				}
			}

			if (part != null) {
				if (IsCollidedToMechanicalModel(shapeScript, part, locationModel)) {
					ChangeParent(part, locationModel);
					Trace.WriteLine(stepId.ToString() + ": Release " + part.CollisionSourceName + ".");
					shapeScript.NextStep++;
				}
			}
		}

		public static void ReleasePallet(IExtendedShapeScript shapeScript, int stepId, IVisualizable robotTool, IShapeBase palletModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if ((robotTool != null) && (palletModel != null) && (locationModel != null)) {
				ShapeRenderInfo pallet = null;
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.Shape == palletModel) && (renderInfo.Parent == robotTool)) {
						pallet = renderInfo;
						break;
					}
				}

				if (pallet != null) {
					if (IsCollidedToMechanicalModel(shapeScript, pallet, locationModel)) {
						ChangePalletParent(pallet, locationModel, renderInfoList);
						Trace.WriteLine(stepId.ToString() + ": Release " + pallet.CollisionSourceName + ".");
						shapeScript.NextStep++;
					}
				}
			}
		}

		public static void ReleasePartOnPallet(IExtendedShapeScript shapeScript, int stepId, IVisualizable robotTool, IShapeBase partModel, IShapeBase palletModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			ShapeRenderInfo part = null;
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Parent == robotTool)) {
					part = renderInfo;
					break;
				}
			}

			if (part != null) {
				ShapeRenderInfo pallet = GetCollidingPallet(shapeScript, part, palletModel, renderInfoList);
				if (pallet != null) {
					ChangeParent(part, pallet.Parent);
					SetParentPalletCSName(part, pallet);
					Trace.WriteLine(stepId.ToString() + ": Release " + part.CollisionSourceName + " on " + pallet.CollisionSourceName + ".");
					shapeScript.NextStep++;
				}
			}
		}

		public static void ReleasePartBySignal(IExtendedShapeScript shapeScript, int stepId, string controllerName, string variableName, IVisualizable robotTool, IShapeBase partModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues) == false) {
				//Release a part.
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.EnablePhysics == false) && (renderInfo.Shape == partModel) && (renderInfo.Parent == robotTool)) {
						if (IsCollidedToMechanicalModel(shapeScript, renderInfo, locationModel)) {
							ChangeParent(renderInfo, locationModel);
							Trace.WriteLine(stepId.ToString() + ": Release and Place " + renderInfo.CollisionSourceName + ".");
							shapeScript.NextStep++;
						}
						else {
							ChangeParent(renderInfo, null);
							Trace.WriteLine(stepId.ToString() + ": Release " + renderInfo.CollisionSourceName + ".");
						}
						return;
					}
				}
			}

			//For a falling part.
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && renderInfo.EnablePhysics && (renderInfo.Shape == partModel)) {
					if (GetPreviousParentIdentifier(renderInfo).Equals(robotTool.Identifier.ToString())) {
						if (renderInfo.IsPhysicsSleeping && IsCollidedToMechanicalModel(shapeScript, renderInfo, locationModel)) {
							ChangeParent(renderInfo, locationModel);
							Trace.WriteLine(stepId.ToString() + ": Place " + renderInfo.CollisionSourceName + ".");
							shapeScript.NextStep++;
							break;
						}
					}
				}
			}
		}

		public static void ReleasePalletBySignal(IExtendedShapeScript shapeScript, int stepId, string controllerName, string variableName, IVisualizable robotTool, IShapeBase palletModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues) == false) {
				//Release a pallet.
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.Parent == robotTool)) {
						if ((renderInfo.Shape == palletModel) || (string.IsNullOrEmpty(GetParentPalletCSName(renderInfo)) == false)) {
							if (IsCollidedToMechanicalModel(shapeScript, renderInfo, locationModel)) {
								ChangePalletParent(renderInfo, locationModel, renderInfoList);
								Trace.WriteLine(stepId.ToString() + ": Release and Place " + renderInfo.CollisionSourceName + ".");
								shapeScript.NextStep++;
							}
							else {
								ChangePalletParent(renderInfo, null, renderInfoList);
								Trace.WriteLine(stepId.ToString() + ": Release " + renderInfo.CollisionSourceName + ".");
							}
							return;
						}
					}
				}
			}

			//For a falling pallet.
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.EnablePhysics && (renderInfo.Shape == palletModel)) {
					if (GetPreviousParentIdentifier(renderInfo).Equals(robotTool.Identifier.ToString())) {
						if (renderInfo.IsPhysicsSleeping && IsCollidedToMechanicalModel(shapeScript, renderInfo, locationModel)) {
							ChangePalletParent(renderInfo, locationModel, renderInfoList);
							Trace.WriteLine(stepId.ToString() + ": Place " + renderInfo.CollisionSourceName + ".");
							shapeScript.NextStep++;
							break;
						}
					}
				}
			}
		}

		public static void ReleasePartOnPalletBySignal(IExtendedShapeScript shapeScript, int stepId, string controllerName, string variableName, IVisualizable robotTool, IShapeBase partModel, IShapeBase palletModel, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			if (stepId != shapeScript.NextStep) return;
			
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (GetBoolVariable(shapeScript, controllerName, variableName, variableValues) == false) {
				//Release a part.
				foreach (var renderInfo in renderInfoList) {
					if (renderInfo.Visible && (renderInfo.EnablePhysics == false) && (renderInfo.Shape == partModel) && (renderInfo.Parent == robotTool)) {
						ShapeRenderInfo pallet = GetCollidingPallet(shapeScript, renderInfo, palletModel, renderInfoList);
						if (pallet != null) {
							ChangeParent(renderInfo, pallet.Parent);
							SetParentPalletCSName(renderInfo, pallet);
							Trace.WriteLine(stepId.ToString() + ": Release and Place " + renderInfo.CollisionSourceName + " on " + pallet.CollisionSourceName + ".");
							shapeScript.NextStep++;
						}
						else {
							ChangeParent(renderInfo, null);
							Trace.WriteLine(stepId.ToString() + ": Release " + renderInfo.CollisionSourceName + ".");
						}
						return;
					}
				}
			}

			//For a falling part.
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && renderInfo.EnablePhysics && (renderInfo.Shape == partModel)) {
					if (renderInfo.IsPhysicsSleeping) {
						if (GetPreviousParentIdentifier(renderInfo).Equals(robotTool.Identifier.ToString())) {
							ShapeRenderInfo pallet = GetCollidingPallet(shapeScript, renderInfo, palletModel, renderInfoList);
							if (pallet != null) {
								ChangeParent(renderInfo, pallet.Parent);
								SetParentPalletCSName(renderInfo, pallet);
								Trace.WriteLine(stepId.ToString() + ": Place " + renderInfo.CollisionSourceName + " on " + pallet.CollisionSourceName + ".");
								shapeScript.NextStep++;
								break;
							}
						}
					}
				}
			}
		}

		public static void SetClampStatus(IExtendedShapeScript shapeScript, string controllerName, string variableName, IVisualizable robotTool, IShapeBase partModel, IEnumerable<ShapeRenderInfo> renderInfoList, IList<Tuple<string, object>> setVariableList = null) {
			bool value = false;

			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(robotTool, "robotTool");
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Shape == partModel)) {
					if (renderInfo.Parent == robotTool) {
						value = true;
						break;
					}
				}
			}

			if (setVariableList != null) {
				var setValue = new Tuple<string, object>(variableName, value);
				setVariableList.Add(setValue);
			}
			else {
				SetBoolVariable(shapeScript, controllerName, variableName, value);
			}
		}

		public static void DetectPartCollision(IExtendedShapeScript shapeScript, IShapeBase partModel, IVisualizable mechanicalModel, int targetStep, string controllerName, string variableName, IEnumerable<ShapeRenderInfo> renderInfoList, IList<Tuple<string, object>> setVariableList = null) {
			if (targetStep != shapeScript.NextStep) return;

			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(mechanicalModel, "mechanicalModel");
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(renderInfoList, "renderInfoList");

			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.CanCollide && (renderInfo.Shape == partModel) && IsCollidedToMechanicalModel(shapeScript, renderInfo, mechanicalModel)) {
					if ((string.IsNullOrEmpty(controllerName) == false) && (string.IsNullOrEmpty(variableName) == false)) {
						if (setVariableList != null) {
							var setValue = new Tuple<string, object>(variableName, true);
							setVariableList.Add(setValue);
						}
						else {
							SetBoolVariable(shapeScript, controllerName, variableName, true);
						}
					}

					Trace.WriteLine(targetStep.ToString() + ": " + renderInfo.CollisionSourceName + " is colliding.");
					break;
				}
			}
		}

		public static void DetectMechanicalComponentsCollision(IExtendedShapeScript shapeScript, string mechanicalModel, int targetStep, string controllerName, string variableName, IList<Tuple<string, object>> setVariableList = null) {
			if (targetStep != shapeScript.NextStep) return;

			CheckNullOrEmptyArgument(mechanicalModel, "mechanicalModel");
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			IEnumerable<string> collisionList = shapeScript.GetCollidingSourceNames(mechanicalModel);

			bool collisionFound = false;
			foreach (var collision in collisionList) {
				Trace.WriteLine(targetStep.ToString() + ": \"" + mechanicalModel + "\" is colliding to \"" + collision + "\".");
				collisionFound = true;
			}

			if (collisionFound && (string.IsNullOrEmpty(controllerName) == false) && (string.IsNullOrEmpty(variableName) == false)) {
				if (setVariableList != null) {
					var setValue = new Tuple<string, object>(variableName, true);
					setVariableList.Add(setValue);
				}
				else {
					SetBoolVariable(shapeScript, controllerName, variableName, true);
				}
			}
		}

		public static bool GetBoolVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<bool>(variableName);
		}
		
		public static bool GetBoolVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, IDictionary<string, object> variableValues) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			if (variableValues != null)
			{
				var values = new Dictionary<string, object>(variableValues, StringComparer.OrdinalIgnoreCase);
				if (values.ContainsKey(variableName) && values[variableName] != null)
				{
					bool boolValue;
					if (bool.TryParse(values[variableName].ToString(), out boolValue))
					{
						return boolValue;
					}
				}
			}
			
			return GetBoolVariable(shapeScript, controllerName, variableName);
		}

		public static void SetBoolVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, bool value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<bool>(variableName, value);
		}

		public static int GetIntegerVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<int>(variableName);
		}

		public static void SetIntegerVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, int value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<int>(variableName, value);
		}
		
		public static void SetRealVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, float value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<float>(variableName, value);
		}

		public static sbyte GetSintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<sbyte>(variableName);
		}
		
		public static void SetSintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, object value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			sbyte setValue;
			if (sbyte.TryParse(value.ToString(), out setValue))
			{
				shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<sbyte>(variableName, setValue);
			}
			else
			{
				var errorMessage = string.Format(
					"Failed to set \"{0}\" to {1}. Please change a value that it can be cast with {2}. ", value, variableName, "sbyte");
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "value", errorMessage);
			}
		}
		
		public static short GetIntVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<short>(variableName);
		}
		
		public static void SetIntVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, object value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			short setValue;
			if (short.TryParse(value.ToString(), out setValue))
			{
				shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<short>(variableName, setValue);
			}
			else
			{
				var errorMessage = string.Format(
					"Failed to set \"{0}\" to {1}. Please change a value that it can be cast with {2}. ", value, variableName, "short");
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "value", errorMessage);
			}
		}
		
		public static int GetDintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<int>(variableName);
		}
		
		public static void SetDintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, object value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			int setValue;
			if (int.TryParse(value.ToString(), out setValue))
			{
				shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<int>(variableName, setValue);
			}
			else
			{
				var errorMessage = string.Format(
					"Failed to set \"{0}\" to {1}. Please change a value that it can be cast with {2}. ", value, variableName, "int");
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "value", errorMessage);
			}
		}
		
		public static long GetLintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<long>(variableName);
		}
		
		public static void SetLintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, object value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			long setValue;
			if (long.TryParse(value.ToString(), out setValue))
			{
				shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<long>(variableName, setValue);
			}
			else
			{
				var errorMessage = string.Format(
					"Failed to set \"{0}\" to {1}. Please change a value that it can be cast with {2}. ", value, variableName, "long");
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "value", errorMessage);
			}
		}
		
		public static byte GetUsintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<byte>(variableName);
		}
		
		public static void SetUsintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, object value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			byte setValue;
			if (byte.TryParse(value.ToString(), out setValue))
			{
				shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<byte>(variableName, setValue);
			}
			else
			{
				var errorMessage = string.Format(
					"Failed to set \"{0}\" to {1}. Please change a value that it can be cast with {2}. ", value, variableName, "byte");
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "value", errorMessage);
			}
		}
		
		public static ushort GetUintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<ushort>(variableName);
		}
		
		public static void SetUintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, object value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			ushort setValue;
			if (ushort.TryParse(value.ToString(), out setValue))
			{
				shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<ushort>(variableName, setValue);
			}
			else
			{
				var errorMessage = string.Format(
					"Failed to set \"{0}\" to {1}. Please change a value that it can be cast with {2}. ", value, variableName, "ushort");
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "value", errorMessage);
			}
		}
		
		public static uint GetUdintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<uint>(variableName);
		}
		
		public static void SetUdintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, object value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			uint setValue;
			if (uint.TryParse(value.ToString(), out setValue))
			{
				shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<uint>(variableName, setValue);
			}
			else
			{
				var errorMessage = string.Format(
					"Failed to set \"{0}\" to {1}. Please change a value that it can be cast with {2}. ", value, variableName, "uint");
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "value", errorMessage);
			}
		}
		
		public static ulong GetUlintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<ulong>(variableName);
		}
		
		public static void SetUlintVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, object value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			ulong setValue;
			if (ulong.TryParse(value.ToString(), out setValue))
			{
				shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<ulong>(variableName, setValue);
			}
			else
			{
				var errorMessage = string.Format(
					"Failed to set \"{0}\" to {1}. Please change a value that it can be cast with {2}. ", value, variableName, "ulong");
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "value", errorMessage);
			}
		}

		public static DateTime GetCurrentControllerTime(IExtendedShapeScript shapeScript, string controllerName, IDictionary<string, object> variableValues) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");

			if (variableValues != null) {
				object value;
				if (variableValues.TryGetValue("_CurrentTime", out value) && value != null && value is DateTime) {
					return (DateTime) value;
				}
			}

			return GetCurrentControllerTime(shapeScript, controllerName);
		}

		public static DateTime GetCurrentControllerTime(IExtendedShapeScript shapeScript, string controllerName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<DateTime>("_CurrentTime");
		}

		public static void InitializeShapeRenderInfo(ShapeRenderInfo renderInfo) {
			CheckNullArgument(renderInfo, "renderInfo");

			renderInfo.EnablePhysics = false;
			renderInfo.Position = new Transform3D(0, 0, 0);
			renderInfo.CanCollide = false;
			renderInfo.Parent = null;
			renderInfo.Visible = false;
			renderInfo.Tag = null;
		}

		public static void InitializePalletShapeRenderInfo(ShapeRenderInfo pallet, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(pallet, "pallet");
			CheckNullArgument(renderInfoList, "renderInfoList");

			//For parts on a pallet.
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Shape != pallet.Shape)) {
					if (GetParentPalletCSName(renderInfo).Equals(pallet.CollisionSourceName)) {
						InitializeShapeRenderInfo(renderInfo);
						Trace.WriteLine("Unload " + renderInfo.CollisionSourceName + " on " + pallet.CollisionSourceName + ".");
					}
				}
			}

			//For a pallet.
			InitializeShapeRenderInfo(pallet);
			Trace.WriteLine("Unload " + pallet.CollisionSourceName + ".");
		}

		public static void DisplayShapeRenderInfo(ShapeRenderInfo renderInfo, Transform3D position, IVisualizable parentModel) {
			CheckNullArgument(renderInfo, "renderInfo");
			CheckNullArgument(position, "position");

			renderInfo.EnablePhysics = false;
			renderInfo.Position = position;
			renderInfo.CanCollide = true;
			ChangeParent(renderInfo, parentModel);
			renderInfo.Visible = true;
			renderInfo.Tag = null;
		}

		public static void ChangeParent(ShapeRenderInfo renderInfo, IVisualizable newParent) {
			CheckNullArgument(renderInfo, "renderInfo");

			if (renderInfo.Parent == newParent) return;

			var childPos = renderInfo.Position;
			var parentPos = new Transform3D(0, 0, 0);
			if (renderInfo.Parent != null) {
				parentPos = renderInfo.Parent.WorldLocation;
				childPos = parentPos * childPos;
			}

			if (renderInfo.EnablePhysics) {
				childPos = renderInfo.PhysicsWorldLocation;
			}

			if (newParent != null) {
				childPos = - newParent.WorldLocation * childPos;
			}

			if (newParent != null) {
				renderInfo.EnablePhysics = false;
			}
			else {
				renderInfo.EnablePhysics = true;
			}
			
			SetPreviousParentIdentifier(renderInfo);
			renderInfo.Parent = newParent;
			renderInfo.Position = childPos;
		}

		public static void ChangePalletParent( ShapeRenderInfo pallet, IVisualizable newParent, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(pallet, "pallet");
			CheckNullArgument(renderInfoList, "renderInfoList");

			//For a pallet.
			ChangeParent(pallet, newParent);

			//For parts on a pallet.
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Shape != pallet.Shape) && GetParentPalletCSName(renderInfo).Equals(pallet.CollisionSourceName)) {
					ChangeParent(renderInfo, newParent);
				}
			}
		}

		public static bool IsCollidedToMechanicalModel(IExtendedShapeScript shapeScript, ShapeRenderInfo renderInfo, IVisualizable mechanicalModel) {
			CheckNullArgument(renderInfo, "renderInfo");

			bool ret = false;

			IEnumerable<string> collisionList = shapeScript.GetCollidingSourceNames(renderInfo.CollisionSourceName);

			foreach (var collision in collisionList) {
				string[] name = collision.Split(',');
				if (name.Length > 0) {
					IVisualizable model = shapeScript.Ace["/" + shapeScript.ApplicationManagerName + "/" + name[0]] as IVisualizable;
					if (model != null) {
						if (renderInfo.Parent != model) {
							if ((model == mechanicalModel) || ((mechanicalModel == null) && ((model is IPartDetectionSensor) == false))) {
								ret = true;
								break;
							}
						}
					}
				}
			}

			return ret;
		}

		public static ShapeRenderInfo GetCollidingPallet(IExtendedShapeScript shapeScript, ShapeRenderInfo part, IVisualizable palletModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(part, "part");
			CheckNullArgument(renderInfoList, "renderInfoList");

			IEnumerable<string> collisionList = shapeScript.GetCollidingSourceNames(part.CollisionSourceName);
			foreach (var pallet in renderInfoList) {
				if (pallet.Visible && (pallet.Shape == palletModel)) {
					foreach (var collision in collisionList) {
						if (pallet.CollisionSourceName.Equals(collision)) {
							return pallet;
						}

					}
				}
			}

			return null;
		}

		public static string GetParentPalletCSName(ShapeRenderInfo renderInfo) {
			CheckNullArgument(renderInfo, "renderInfo");

			string CSName = string.Empty;
			var tag = renderInfo.Tag as string;
			if (tag != null) {
				string[] parentData = tag.Split(',');
				if (parentData.Length > 0) {
					CSName = parentData[0];
				}
			}

			return CSName;
		}

		public static void SetParentPalletCSName(ShapeRenderInfo part, ShapeRenderInfo pallet) {
			CheckNullArgument(part, "part");

			string partTag = ",";
			if (pallet != null) {
				partTag = pallet.CollisionSourceName + ",";
			}
			var tag = part.Tag as string;
			if (tag != null) {
				string[] parentData = tag.Split(',');
				if (parentData.Length > 1) {
					partTag += parentData[1];
				}
			}

			part.Tag = partTag;
		}

		public static string GetPreviousParentIdentifier(ShapeRenderInfo renderInfo) {
			CheckNullArgument(renderInfo, "renderInfo");

			string identifier = string.Empty;
			var tag = renderInfo.Tag as string;
			if (tag != null) {
				string[] parentData = tag.Split(',');
				if (parentData.Length > 1) {
					identifier = parentData[1];
				}
			}

			return identifier;
		}

		public static void SetPreviousParentIdentifier(ShapeRenderInfo renderInfo) {
			CheckNullArgument(renderInfo, "renderInfo");

			string partTag = ",";
			if (renderInfo.Parent != null) {
				partTag += renderInfo.Parent.Identifier.ToString();
			}

			var tag = renderInfo.Tag as string;
			if (tag != null) {
				string[] parentData = tag.Split(',');
				if (parentData.Length > 0) {
					partTag = parentData[0] + partTag;
				}
			}

			renderInfo.Tag = partTag;
		}

		public static void SetControllerBoolInfo(IExtendedShapeScript shapeScript, string controllerName, string variableName, Transform3D position, string name = "") {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(position, "position");
			
			var loadingInfo = string.Format("{0};{1};{2};{3},", controllerName, variableName, position.ToString(), name);
			if (shapeScript.ControllerBoolInfo == null) {
				shapeScript.ControllerBoolInfo = new StringBuilder(loadingInfo);
			}
			else {
				shapeScript.ControllerBoolInfo.Append(loadingInfo);
			}
		}

		public static bool GetControllerBoolInfo(IExtendedShapeScript shapeScript, string controllerName, string variableName, Transform3D position, string name = "") {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(position, "position");

			var ret = false;

			if (shapeScript.ControllerBoolInfo != null) {
				var loadingInfo = string.Format("{0};{1};{2};{3}", controllerName, variableName,position.ToString(), name);
				string[] infoList = shapeScript.ControllerBoolInfo.ToString().Split(',');
				for (int i = 0; i < infoList.Length; i++) {
					if (infoList[i].Equals(loadingInfo)) {
						ret = true;
						break;
					}
				}
			}

			return ret;
		}

		public static void RemoveControllerBoolInfo(IExtendedShapeScript shapeScript, string controllerName, string variableName, Transform3D position, string name = "") {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(position, "position");

			if ((shapeScript.ControllerBoolInfo != null) && (string.IsNullOrEmpty(shapeScript.ControllerBoolInfo.ToString()) == false)) {
				var loadingInfo = string.Format("{0};{1};{2};{3}", controllerName, variableName, position.ToString(), name);
				shapeScript.ControllerBoolInfo = shapeScript.ControllerBoolInfo.Replace(loadingInfo, string.Empty);
			}
		}
		
		public static void CreateMultipleObjectsForCameraLatch(IExtendedShapeScript shapeScript, IShapeBase objectModel, string name, string collisionGroup, int count, List<ShapeRenderInfo> renderInfoList, string objectName, bool isDisplayNumber = false) {
			CreateMultipleObjectsForPMLatch(shapeScript, objectModel, name, collisionGroup, count, true, renderInfoList, objectName, isDisplayNumber);
		}
		
		public static void CreateMultipleObjectsForNonCameraLatch(IExtendedShapeScript shapeScript, IShapeBase objectModel, string name, string collisionGroup, int count, List<ShapeRenderInfo> renderInfoList, bool isDisplayNumber = false) {
			CreateMultipleObjectsForPMLatch(shapeScript, objectModel, name, collisionGroup, count, false, renderInfoList, null, isDisplayNumber);
		}
		
		public static void MovePartOnPackManagerSensorLatchBelt(IExtendedShapeScript shapeScript, IProcessManager processManager, IBelt beltModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			MoveObjectOnSensorLatchBelt(shapeScript, processManager, beltModel, renderInfoList, false);
		}
		
		public static void MovePalletOnPackManagerSensorLatchBelt(IExtendedShapeScript shapeScript, IProcessManager processManager, IBelt beltModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			MoveObjectOnSensorLatchBelt(shapeScript, processManager, beltModel, renderInfoList, true);
		}
		
		public static void MoveObjectOnSensorLatchBelt(IExtendedShapeScript shapeScript, IProcessManager processManager, IBelt belt, IEnumerable<ShapeRenderInfo> renderInfoList, bool isPallet = false) {
			CheckNullArgument(processManager, "processManager");
			CheckNullArgument(belt, "belt");
			CheckNullArgument(renderInfoList, "renderInfoList");
			
			int encoderCount = 0;
			double encoderRatio = 0.0;
			int encoderNumber = 0;
			int latchInput = 0;

			if (GetEncoderParameters(processManager, belt, out encoderNumber, out encoderCount, out encoderRatio, out latchInput)) {
				if ((shapeScript.PreviousEncoderCount != -1) && (encoderCount != shapeScript.PreviousEncoderCount)) {
					var latchPosition = GetSensorLatchPosition(processManager.Calibrations, belt);
					double diff = (encoderCount - shapeScript.PreviousEncoderCount) * encoderRatio;

					if (isPallet) {
						MovePalletOnProcessManagerBelt(shapeScript, belt, latchPosition, encoderNumber, latchInput, encoderCount, diff, renderInfoList);
					}
					else {
						MovePartOnProcessManagerBelt(shapeScript, belt, latchPosition, encoderNumber, latchInput, encoderCount, diff, renderInfoList);
					}
				}
			}

			shapeScript.PreviousEncoderCount = encoderCount;
		}

		public static void SetPackManagerPartPosition(IExtendedShapeScript shapeScript, string controllerName, string variableDX, string variableDY, string variableDZ, IShapeBase partModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullArgument(renderInfoList, "renderInfoList");

			foreach (var renderInfo in renderInfoList) {
				if (!renderInfo.Visible && renderInfo.Shape != partModel) {
					continue;
				}

				// The coordinate values are set to controller simulator when part instances are created by Pack Manager. 
				// Please change the condition the following if you want to decrease data traffic. 
				// if (renderInfo.PackManagerSetting.InstanceState != PackManagerRenderInfoInstanceState.InProgress)
				if (renderInfo.PackManagerSetting.InstanceState == PackManagerRenderInfoInstanceState.NotYet) {
					continue;
				}

				var position = renderInfo.Position;
				if (renderInfo.Parent != null)
				{
					position = renderInfo.Parent.WorldLocation * position;		// Convert to world coordinate. 
				}
					
				var arrayNumber = string.Format("[{0}]", renderInfo.PackManagerSetting.InstanceNumber);
				if (!string.IsNullOrEmpty(variableDX)) {
					SetRealVariable(shapeScript, controllerName, variableDX + arrayNumber, (float)position.DX);
				}

				if (!string.IsNullOrEmpty(variableDY)) {
					SetRealVariable(shapeScript, controllerName, variableDY + arrayNumber, (float)position.DY);
				}

				if (!string.IsNullOrEmpty(variableDZ)) {
					SetRealVariable(shapeScript, controllerName, variableDZ + arrayNumber, (float)position.DZ);
				}
			}
		}

		public static void MovePartOnProcessManagerBelt(IExtendedShapeScript shapeScript, IBelt belt, Transform3D latchPosition, int encoderNumber, int latchInput, int encoderCount, double distance, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(belt, "belt");
			CheckNullArgument(latchPosition, "latchPosition");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (belt.BeltControl == null) {
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "belt", "belt.BeltControl is null");
			}

			if (belt.BeltControl.Controller == null) {
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "belt", "belt.BeltControl.Controller is null");
			}

			var adeptController = belt.BeltControl.Controller;
			foreach (var renderInfo in renderInfoList) {
				if (renderInfo.Visible && (renderInfo.Parent == belt) && (renderInfo.PackManagerSetting.InstanceState != PackManagerRenderInfoInstanceState.InProgress)) {
					if (string.IsNullOrEmpty(GetParentPalletCSName(renderInfo))) {
						renderInfo.Position = renderInfo.Position.Shift(distance, 0, 0);

						if ((renderInfo.Position.DX >= latchPosition.DX) && (renderInfo.PackManagerSetting.InstanceState == PackManagerRenderInfoInstanceState.NotYet)) {
							if (renderInfo.PackManagerSetting.InstanceNumber == -1) {
								renderInfo.PackManagerSetting.InstanceNumber = shapeScript.PackManagerInstanceNumber;
								renderInfo.DisplayNumber = shapeScript.PackManagerInstanceNumber;
								shapeScript.PackManagerInstanceNumber++;
								Debug.WriteLine("Creating latch for Part(" + renderInfo.PackManagerSetting.InstanceNumber.ToString() + ") is at (" + renderInfo.Position.ToString() + ").");
								renderInfo.Position = renderInfo.Parent.WorldLocation * renderInfo.Position;
								renderInfo.Parent = null;
								adeptController.CreateLatch(encoderNumber, latchInput, encoderCount);
								Debug.WriteLine("Created latch for Part(" + renderInfo.PackManagerSetting.InstanceNumber.ToString() + ") is at (" + renderInfo.Position.ToString() + ").");
							}
						}
					}
				}
			}
		}

		public static void MovePalletOnProcessManagerBelt(IExtendedShapeScript shapeScript, IBelt belt, Transform3D latchPosition, int encoderNumber, int latchInput, int encoderCount, double distance, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(belt, "belt");
			CheckNullArgument(latchPosition, "latchPosition");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (belt.BeltControl == null) {
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "belt", "belt.BeltControl is null");
			}

			if (belt.BeltControl.Controller == null) {
				throw new TraceMessageArgumentException(TraceMessageErrorCode.InvalidArgument, "belt", "belt.BeltControl.Controller is null");
			}

			var adeptController = belt.BeltControl.Controller;
			foreach (var pallet in renderInfoList) {
				if (pallet.Visible && (pallet.Parent == belt)) {
					if (string.IsNullOrEmpty(GetParentPalletCSName(pallet))) {
						pallet.Position = pallet.Position.Shift(distance, 0, 0);

						List<ShapeRenderInfo> partsOnPallet = new List<ShapeRenderInfo>();
						foreach (var part in renderInfoList) {
							if (part.Visible && (part.Parent == belt) && (part.PackManagerSetting.InstanceState != PackManagerRenderInfoInstanceState.InProgress)) {
								if (GetParentPalletCSName(part).Equals(pallet.CollisionSourceName)) {
									part.Position = part.Position.Shift(distance, 0, 0);
									partsOnPallet.Add(part);
								}
							}
						}

						if ((pallet.Position.DX >= latchPosition.DX)) {
							bool needLatch = false;
							foreach (var part in partsOnPallet) {
								if ((part.PackManagerSetting.InstanceNumber == -1) && (part.PackManagerSetting.InstanceState == PackManagerRenderInfoInstanceState.NotYet)) {
									part.PackManagerSetting.InstanceNumber = shapeScript.PackManagerInstanceNumber;
									part.DisplayNumber = shapeScript.PackManagerInstanceNumber;
									shapeScript.PackManagerInstanceNumber++;
									part.Position = part.Parent.WorldLocation * part.Position;
									part.Parent = null;
									SetParentPalletCSName(part, null);
									needLatch = true;
								}
							}

							if (needLatch) {
								adeptController.CreateLatch(encoderNumber, latchInput, encoderCount);
								Debug.WriteLine("Created latch for Pallet (" + pallet.Position.ToString() + ").");
							}
						}
					}
				}
			}
		}
		
		public static Transform3D GetSensorLatchPosition(ICalibrationCollection calibrations, IBelt belt) {
			CheckNullArgument(calibrations, "calibrations");
			CheckNullArgument(belt, "belt");

			var position = new Transform3D(0, 0, 0);
			var BLcalibrations = calibrations.GetRobotToBeltLatchCalibrations();
			if (BLcalibrations.Length > 0) {
				var sensorOffset = BLcalibrations[0].RobotToSensorOffset;
				var Bcalibrations = calibrations.GetRobotToBeltCalibrations();
				if ((Bcalibrations.Length > 0) && (Bcalibrations[0].Robot != null)) {
					var beltTransform = Bcalibrations[0].BeltTransform;
					beltTransform = ConvertToRobotCoordinate(Bcalibrations[0].Robot, beltTransform);
					var latchPos = beltTransform * sensorOffset;
					position = - belt.WorldLocation * latchPos;
				}
			}

			return position;
		}

		public static Transform3D ConvertToRobotCoordinate(IAdeptRobot robot, Transform3D targetLocation) {
			CheckNullArgument(robot, "robot");
			CheckNullArgument(targetLocation, "targetLocation");

			var position = new Transform3D(0, 0, 0);

			if (robot.Parent == null) {
				position = robot.OffsetFromParent * targetLocation;
			}
			else {
				position = robot.Parent.WorldLocation * targetLocation;
			}

			return position;
		}

		public static bool GetEncoderParameters(IProcessManager processManager, IBelt belt, out int number, out int count, out double ratio, out int latchInput) {
			CheckNullArgument(processManager, "processManager");
			CheckNullArgument(belt, "belt");

			bool ret = false;
			number = 0;
			count = 0;
			ratio = 0.0;
			latchInput = 0;

			if ((processManager.Calibrations != null) && (belt.BeltControl != null) && (belt.BeltControl.Controller != null)) {
				if ((belt.BeltControl.Controller.EnabledEncoderCount > 0) && (belt.Encoders != null)) {
					count = belt.BeltControl.Controller.GetEncoderPosition(0);	// Fixed to 0.

					var beltEncoderControllerConnections = belt.Encoders.FirstOrDefault().ControllerConnections;
					if ((beltEncoderControllerConnections != null) && (beltEncoderControllerConnections.Length > 0)) {
						ratio = beltEncoderControllerConnections[0].MMPerEncoderRatio;
						latchInput = beltEncoderControllerConnections[0].LatchInputNumbers[0];

						var BLcalibrations = processManager.Calibrations.GetRobotToBeltLatchCalibrations();
						if (BLcalibrations.Length > 0) {
							number = BLcalibrations[0].EncoderNumber;
							ret = true;
						}

						if (ret == false) {
							var BCcalibrations = processManager.Calibrations.GetRobotToBeltCameraCalibrations();
							if (BCcalibrations.Length > 0) {
								number = BCcalibrations[0].EncoderNumber;
								ret = true;
							}
						}
					}
				}
			}

			return ret;
		}
		
		public static void UpdatePartDataFromPackManager(IShapeBase partModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(locationModel, "locationModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			foreach (var renderInfo in renderInfoList) {
				if ((renderInfo.Shape == partModel) && (renderInfo.PackManagerSetting.InstanceState == PackManagerRenderInfoInstanceState.Finished)) {
					if (renderInfo.Parent == null) {
						renderInfo.PackManagerSetting.InstanceNumber = -1;
						renderInfo.Position = - locationModel.WorldLocation * renderInfo.Position;
						renderInfo.Parent = locationModel;
						Trace.WriteLine("Update " + renderInfo.CollisionSourceName + " data from Application Sample.");
					}
				}
			}
		}
		
		public static void UpdatePartDataOnPalletFromPackManager(IExtendedShapeScript shapeScript, IShapeBase partModel, IShapeBase palletModel, IVisualizable locationModel, IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(partModel, "partModel");
			CheckNullArgument(palletModel, "palletModel");
			CheckNullArgument(renderInfoList, "renderInfoList");

			foreach (var renderInfo in renderInfoList) {
				if ((renderInfo.Shape == partModel) && (renderInfo.PackManagerSetting.InstanceState == PackManagerRenderInfoInstanceState.Finished)) {
					if (renderInfo.Parent == null) {
						ShapeRenderInfo pallet = GetCollidingPallet(shapeScript, renderInfo, palletModel, renderInfoList);
						if (pallet != null) {
							SetParentPalletCSName(renderInfo, pallet);
							renderInfo.PackManagerSetting.InstanceNumber = -1;
							renderInfo.Position = - pallet.Parent.WorldLocation * renderInfo.Position;
							renderInfo.Parent = pallet.Parent;
							Trace.WriteLine("Update " + renderInfo.CollisionSourceName + " data from Application Sample.");
						}
					}
				}
			}
		}

		public static void CreateMultipleObjectsForPMLatch(IExtendedShapeScript shapeScript, IShapeBase objectModel, string name, string collisionGroup, int count, bool isVision, List<ShapeRenderInfo> renderInfoList, string objectName = null, bool isDisplayNumber = false) {
			CheckNullArgument(objectModel, "objectModel");
			CheckNullArgument(name, "name");
			CheckNullOrEmptyArgument(collisionGroup, "collisionGroup");
			CheckNullArgument(renderInfoList, "renderInfoList");

			int index = 0;

			if (!isVision) {
				index = shapeScript.PackManagerInstanceNumber;
			}

			for (int i = 0; i < count; i++) {
				var eachName = name + "_" + i.ToString();
				CreateObject(objectModel, eachName, collisionGroup, renderInfoList, index, isVision, objectName, isDisplayNumber);
				index++;
			}

			if (isVision) {
				objectModel.Tag = index;
			}
			else {
				shapeScript.PackManagerInstanceNumber = index;
			}
		}
		
		public static void HidePackManagerNotPickedRenderInfo(IEnumerable<ShapeRenderInfo> renderInfoList) {
			CheckNullArgument(renderInfoList, "renderInfoList");

			var notPickedRenderInfo = renderInfoList.Where(renderInfo => 
				renderInfo.PackManagerSetting.InstanceState == PackManagerRenderInfoInstanceState.NotPicked && renderInfo.Visible);
			foreach (var renderInfo in notPickedRenderInfo) {
				renderInfo.Visible = false;
			}
		}

		public static IList<Tuple<string, Type>> CreateGetVariableList() {
			return new DataTypeList<Tuple<string, Type>>();
		}

		public static bool AddToGetVariableList(string variableName, string controllerDataType, IList<Tuple<string, Type>> getVariableList, bool isShowErrorMessage = true) {
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(controllerDataType, "controllerDataType");
			CheckNullArgument(getVariableList, "getVariableList");

			// Remove an index from STRING variable
			const char bracketChar = '[';
			var bracketIndex = controllerDataType.IndexOf(bracketChar);
			if (bracketIndex != -1) {
				controllerDataType = controllerDataType.Substring(0, bracketIndex);
			}
			
			if (VplusDataTypeList.ContainsKey(controllerDataType) && getVariableList.Any(data => data.Item1 == variableName) == false) {
				CheckValidDataTypeList(getVariableList, ControllerType.Vplus, variableName);

				getVariableList.Add(new Tuple<string, Type>(variableName, VplusDataTypeList[controllerDataType]));
				
				return true;
			}

			if (IecDataTypeList.ContainsKey(controllerDataType) && getVariableList.Any(data => data.Item1 == variableName) == false) {
				CheckValidDataTypeList(getVariableList, ControllerType.Iec, variableName);

				getVariableList.Add(new Tuple<string, Type>(variableName, IecDataTypeList[controllerDataType]));
				
				return true;
			}
			
			if (isShowErrorMessage) {
				Trace.WriteLine(string.Format("Failed to add \"{0}\" to the get variable list. ", variableName));
			}
			
			return false;
		}

		public static IDictionary<string, object> GetVariableValues(IExtendedShapeScript shapeScript, string controllerName, IList<Tuple<string, Type>> getVariableList) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(getVariableList, "getVariableList");

			return new Dictionary<string, object>(shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValues(getVariableList), StringComparer.OrdinalIgnoreCase);
		}

		public static object GetValueFromVariableValueList(string variableName, IDictionary<string, object> variableValueList) {
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullArgument(variableValueList, "variableValueList");

			if (variableValueList.ContainsKey(variableName)) {
				return variableValueList[variableName];
			}
			
			return null;
		}

		public static IList<Tuple<string, object>> CreateSetVariableList() {
			return new DataTypeList<Tuple<string, object>>();
		}

		public static bool AddToSetVariableList(string variableName, string controllerDataType, object setValue, IList<Tuple<string, object>> setVariableList, bool isShowErrorMessage = true) {
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(controllerDataType, "controllerDataType");
			CheckNullArgument(setValue, "setValue");
			CheckNullArgument(setVariableList, "setVariableList");

			// Remove an index from STRING variable
			const char bracketChar = '[';
			var bracketIndex = controllerDataType.IndexOf(bracketChar);
			if (bracketIndex != -1) {
				controllerDataType = controllerDataType.Substring(0, bracketIndex);
			}
			
			if (VplusDataTypeList.ContainsKey(controllerDataType) && setVariableList.Any(data => data.Item1 == variableName) == false) {
				CheckValidDataTypeList(setVariableList, ControllerType.Vplus, variableName);
				try {
					var newData = new Tuple<string, object>(variableName, Convert.ChangeType(setValue, VplusDataTypeList[controllerDataType]));
					setVariableList.Add(newData);
					
					return true;
				}
				catch (Exception) {
					throw new TraceMessageVariableException(TraceMessageErrorCode.VariableCannotAddToList, variableName, 
						string.Format("Failed to add \"{0}\" to the set variable list. The value \"{1}\" is invalid. ", variableName, setValue));
				}
			}

			if (IecDataTypeList.ContainsKey(controllerDataType) && setVariableList.Any(data => data.Item1 == variableName) == false) {
				CheckValidDataTypeList(setVariableList, ControllerType.Iec, variableName);
				try {
					var newData = new Tuple<string, object>(variableName, Convert.ChangeType(setValue, IecDataTypeList[controllerDataType]));
					setVariableList.Add(newData);
					
					return true;
				}
				catch (Exception) {
					throw new TraceMessageVariableException(TraceMessageErrorCode.VariableCannotAddToList, variableName, 
						string.Format("Failed to add \"{0}\" to the set variable list. The value \"{1}\" is invalid. ", variableName, setValue));
				}
			}
			
			if (isShowErrorMessage) {
				Trace.WriteLine(string.Format("Failed to add \"{0}\" to the set variable list. ", variableName));
			}
			
			return false;
		}

		public static void SetVariableValues(IExtendedShapeScript shapeScript, string controllerName, IList<Tuple<string, object>> setVariableList) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(setVariableList, "setVariableList");
			
			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValues(setVariableList);
		}

		public static void MoveObjectOnMechanicalConveyor(IExtendedShapeScript shapeScript, IMechanicalConveyor mechanicalConveyor, IEnumerable<ShapeRenderInfo> renderInfoList, IDictionary<string, object> variableValues = null) {
			CheckNullArgument(mechanicalConveyor, "mechanicalConveyor");
			CheckNullArgument(renderInfoList, "renderInfoList");

			if (mechanicalConveyor.IsConveyorMechanics == false) {
				return;
			}

			var axisValue = mechanicalConveyor.GetConveyorAxisVariable(variableValues);
			if (mechanicalConveyor.IsMoveConveyor == false) {
				mechanicalConveyor.StartConveyor(axisValue);
			}

			foreach (var pallet in renderInfoList) {
				if (pallet.Visible == false || pallet.Parent != mechanicalConveyor 
						|| string.IsNullOrEmpty(GetParentPalletCSName(pallet)) == false) {
					continue;
				}

				if (IsCollidedToMechanicalModel(shapeScript, pallet, null)) {
					continue;
				}

				//Move a pallet or a part.
				var palletPos = pallet.Position;
				pallet.Position = mechanicalConveyor.CreateWorkPositionOnConveyor(palletPos, axisValue);

				//Move parts on a pallet.
				foreach (var part in renderInfoList)
				{
					if (part.Visible == false || part.Parent != mechanicalConveyor ||
						GetParentPalletCSName(part).Equals(pallet.CollisionSourceName) == false) {
						continue;
					}

					var partPos = part.Position;
					part.Position = mechanicalConveyor.CreateWorkPositionOnConveyor(partPos, axisValue);
				}
			}

			mechanicalConveyor.ConveyorPrevAxisValue = axisValue;
		}

		public static void InitializeMechanicalConveyor(IMechanicalConveyor mechanicalConveyor) {
			CheckNullArgument(mechanicalConveyor, "mechanicalConveyor");
			mechanicalConveyor.InitializeConveyorMoveStatus();
		}

		public static byte[] GetByteArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<byte[]>(variableName);
		}

		public static void SetByteArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, byte[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<byte[]>(variableName, value);
		}

		public static bool[] GetBoolArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<bool[]>(variableName);
		}

		public static void SetBoolArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, bool[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<bool[]>(variableName, value);
		}

		public static sbyte[] GetSintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<sbyte[]>(variableName);
		}

		public static void SetSintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, sbyte[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<sbyte[]>(variableName, value);
		}

		public static short[] GetIntArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<short[]>(variableName);
		}

		public static void SetIntArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, short[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<short[]>(variableName, value);
		}
		
		public static int[] GetIntegerArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<int[]>(variableName);
		}

		public static void SetIntegerArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, int[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<int[]>(variableName, value);
		}

		public static int[] GetDintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<int[]>(variableName);
		}

		public static void SetDintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, int[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<int[]>(variableName, value);
		}

		public static long[] GetLintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<long[]>(variableName);
		}

		public static void SetLintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, long[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<long[]>(variableName, value);
		}

		public static byte[] GetUsintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<byte[]>(variableName);
		}

		public static void SetUsintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, byte[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<byte[]>(variableName, value);
		}

		public static ushort[] GetUintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<ushort[]>(variableName);
		}

		public static void SetUintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, ushort[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<ushort[]>(variableName, value);
		}

		public static uint[] GetUdintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<uint[]>(variableName);
		}

		public static void SetUdintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, uint[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<uint[]>(variableName, value);
		}

		public static ulong[] GetUlintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<ulong[]>(variableName);
		}

		public static void SetUlintArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, ulong[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<ulong[]>(variableName, value);
		}
		
		public static ushort[] GetWordArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<ushort[]>(variableName);
		}

		public static void SetWordArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, ushort[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<ushort[]>(variableName, value);
		}

		public static uint[] GetDwordArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<uint[]>(variableName);
		}

		public static void SetDwordArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, uint[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<uint[]>(variableName, value);
		}

		public static ulong[] GetLwordArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<ulong[]>(variableName);
		}

		public static void SetLwordArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, ulong[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<ulong[]>(variableName, value);
		}

		public static float[] GetRealArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<float[]>(variableName);
		}

		public static void SetRealArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, float[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<float[]>(variableName, value);
		}

		public static double[] GetLrealArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<double[]>(variableName);
		}

		public static void SetLrealArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, double[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<double[]>(variableName, value);
		}

		public static string[] GetStringArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");

			return shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).GetValue<string[]>(variableName);
		}

		public static void SetStringArrayVariable(IExtendedShapeScript shapeScript, string controllerName, string variableName, string[] value) {
			CheckNullOrEmptyArgument(controllerName, "controllerName");
			CheckNullOrEmptyArgument(variableName, "variableName");
			CheckNullOrEmptyArgument(value, "value");

			shapeScript.Ace.GetVariablePresenter(@"\" + controllerName).SetValue<string[]>(variableName, value);
		}
	}
}